//
//  OpenCVWrapperX.h
//  ReceiptX
//
//  Created by Naveed Iqbal on 15/01/2020.
//  Copyright © 2020 Naveed Iqbal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject
+(NSMutableArray *) getLargestSquarePoints: (UIImage *) image : (CGSize) size;
+(UIImage *) getTransformedImage: (CGFloat) newWidth : (CGFloat) newHeight : (UIImage *) origImage : (CGPoint [4]) corners : (CGSize) size;
+(BOOL) checkForBurryImage:(UIImage *) image;

@end

