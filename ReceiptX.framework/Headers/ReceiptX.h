//
//  ReceiptX.h
//  ReceiptX
//
//  Created by Naveed Iqbal on 15/01/2020.
//  Copyright © 2020 Naveed Iqbal. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ReceiptX.
FOUNDATION_EXPORT double ReceiptXVersionNumber;

//! Project version string for ReceiptX.
FOUNDATION_EXPORT const unsigned char ReceiptXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ReceiptX/PublicHeader.h>

#import <ReceiptX/iCarousel.h>
#import <ReceiptX/OpenCVWrapper.h>
#import <ReceiptX/ImageSlicing.h>


