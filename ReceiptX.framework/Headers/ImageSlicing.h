//
//  ImageSlicing.h
//  ReceiptX
//
//  Created by Naveed Iqbal on 16/02/2020.
//  Copyright © 2020 Naveed Iqbal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageSlicing : NSObject

-(NSMutableArray *)getImagesFromImage:(UIImage *)image withRow:(NSInteger)rows withColumn:(NSInteger)columns;

@end

NS_ASSUME_NONNULL_END

