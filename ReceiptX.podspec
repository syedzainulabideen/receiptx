#
#  Be sure to run `pod spec lint ReceiptX.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "ReceiptX"
  spec.version      = "0.1.6"
  spec.summary      = "A ReceiptX"
  spec.description  = <<-DESC 
		VisionX receipt framework
                   DESC

    spec.homepage     = "https://github.com/syedzainulabideen/ReceiptX"
    spec.license      = "MIT"
    spec.author             = { "Syed Zain ul Abideen" => "syed.zain@visionx.io" }
 
    spec.platform     = :ios
    spec.platform     = :ios, "11.0"
    spec.ios.deployment_target = "11.0"
    spec.ios.vendored_frameworks = 'ReceiptX.framework'
    spec.static_framework = true
    spec.source       = { :git => "https://gitlab.com/syedzainulabideen/receiptx.git" }
spec.source_files  = "ReceiptX/**/*.{h,m,swift}"
  spec.dependency 'OpenCV2'
  spec.dependency 'TensorFlowLiteC'
  #spec.dependency 'Firebase'
  #spec.dependency 'FirebaseCore'
  #spec.dependency 'FirebaseMLVision'
  #spec.dependency 'FirebaseMLVisionTextModel'

end
